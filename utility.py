import base64
import io
import os
import os.path as osp
from natsort import natsorted
from diffusers import (
    StableDiffusionPipeline,
    EulerDiscreteScheduler,
    EulerAncestralDiscreteScheduler,
    DPMSolverSinglestepScheduler,
    DPMSolverMultistepScheduler,
    DPMSolverSDEScheduler,
    UniPCMultistepScheduler
)


config = dict(
    num_train_timesteps=1000,
    beta_start=0.00085,
    beta_end=0.012,
    beta_schedule='scaled_linear',
    trained_betas=None,
    clip_sample=False,
    set_alpha_to_one=False,
    steps_offset=1,
    prediction_type='epsilon',
    thresholding=False,
    dynamic_thresholding_ratio = 0.995,
    clip_sample_range = 1.0,
    sample_max_value = 1.0,
    timestep_spacing = 'leading',
    rescale_betas_zero_snr = False,
    interpolation_type = 'linear',
    skip_prk_steps = True
)


def get_files(src, pref, ext=None):
    return [(f.name, f.path) for f in sorted(os.scandir(src), key=lambda f: f.name) if f.is_file() and f.name.startswith(pref) and f.name.endswith(ext)]


def load_image(fil):
    return base64.b64encode(open(fil, 'rb').read()).decode('utf-8')


def save_image(fil, txt):
    io.BytesIO(base64.b64decode(txt)).save(fil)


def load_image(fil):
    return base64.b64encode(open(fil, 'rb').read()).decode('utf-8')


def save_image(fil, txt):
    io.BytesIO(base64.b64decode(txt)).save(fil)


def get_ckpt(mask, short=False, dirs=['models/checkpoints', 'models/loras']):
    if not mask:
        return None
    outs = []
    for src in dirs:
        for f in os.scandir(src):
            if f.is_file():
                if mask in f.path:
                    if short:
                        outs += [f.name]
                    else:
                        outs += [f.path]
    return natsorted(outs)[0] if outs else None


def get_ckpts(inp, short=False):
    if ',' in inp:
        inps = inp.split(',')
        outs = []
        for inp in inps:
            outs += [get_ckpt(inp, short)]
    else:
        outs = [get_ckpt(inp, short)]
    return outs


def get_sampler(sampler):
    if sampler == 'euler_a':
        out = 'euler_ancestral'
    else:
        out = req.sampler
    return out


def get_size(txt):
    size = txt.split(',')
    return (int(size[0]), int(size[1]))


def get_seed(num):
    return num if num > 0 else random.randint(1, 2**64)


def get_sampler(sampler):
    if sampler == 'euler':
        '''
        config = dict(
            beta_end = 0.012,
            beta_schedule = 'scaled_linear',
            beta_start = 0.00085,
            clip_sample = False,
            clip_sample_range = 1.0,
            dynamic_thresholding_ratio = 0.995,
            interpolation_type = 'linear',
            num_train_timesteps = 1000,
            prediction_type = 'epsilon',
            rescale_betas_zero_snr = False,
            sample_max_value = 1.0,
            set_alpha_to_one = False,
            sigma_max = None,
            sigma_min = None,
            skip_prk_steps = True,
            steps_offset = 1,
            thresholding = False,
            timestep_spacing = 'linspace',
            timestep_type = 'discrete',
            trained_betas = None,
            use_karras_sigmas = False
        )
        '''
        output = EulerDiscreteScheduler.from_config(config)
    elif sampler == 'euler_ancestral':
        '''
        config = dict(
            num_train_timesteps = 1000,
            beta_start = 0.0001,
            beta_end = 0.02,
            beta_schedule = 'linear',
            trained_betas = None,
            prediction_type = 'epsilon',
            timestep_spacing = 'linspace',
            steps_offset = 0,
            rescale_betas_zero_snr = False
        )
        config = {
            'num_train_timesteps': 1000,
            'beta_start': 0.00085,
            'beta_end': 0.012,
            'beta_schedule': 'scaled_linear',
            'trained_betas': None,
            'clip_sample': False,
            'set_alpha_to_one': False,
            'steps_offset': 1,
            'prediction_type': 'epsilon',
            'thresholding': False,
            'dynamic_thresholding_ratio': 0.995,
            'clip_sample_range': 1.0,
            'sample_max_value': 1.0,
            'timestep_spacing': 'leading',
            'rescale_betas_zero_snr': False,
            'interpolation_type': 'linear',
            'skip_prk_steps': True
        }
        '''
        output = EulerAncestralDiscreteScheduler.from_config(config)
    elif sampler == 'dpm_2':
        '''
        config = dict(
            num_train_timesteps = 1000,
            beta_start = 0.0001,
            beta_end = 0.02,
            beta_schedule = 'linear',
            trained_betas = None,
            solver_order = 2,
            prediction_type = 'epsilon',
            thresholding = False,
            dynamic_thresholding_ratio = 0.995,
            sample_max_value = 1.0,
            algorithm_type = 'dpmsolver++',
            solver_type = 'midpoint',
            lower_order_final = False,
            use_karras_sigmas = False,
            final_sigmas_type = 'zero',
            lambda_min_clipped = -float('inf'),
            variance_type = None
        )
        '''
        output = DPMSolverSinglestepScheduler.from_config(config)
    elif sampler == 'dpmpp_2m':
        '''
        config = dict(
            num_train_timesteps = 1000,
            beta_start = 0.0001,
            beta_end = 0.02,
            beta_schedule = 'linear',
            trained_betas = None,
            solver_order = 2,
            prediction_type = 'epsilon',
            thresholding = False,
            dynamic_thresholding_ratio = 0.995,
            sample_max_value = 1.0,
            algorithm_type = 'dpmsolver++',
            solver_type = 'midpoint',
            lower_order_final = True,
            euler_at_final = False,
            use_karras_sigmas = False,
            use_lu_lambdas = False,
            final_sigmas_type = 'zero',  # 'zero', 'sigma_min'
            lambda_min_clipped = -float('inf'),
            variance_type = None,
            timestep_spacing = 'linspace',
            steps_offset = 0,
        )
        '''
        output = DPMSolverMultistepScheduler.from_config(config)
    elif sampler == 'unipc':
        '''
        config = dict(
            num_train_timesteps: int = 1000,
            beta_start: float = 0.0001,
            beta_end: float = 0.02,
            beta_schedule: str = "linear",
            trained_betas: Optional[Union[np.ndarray, List[float]]] = None,
            solver_order: int = 2,
            prediction_type: str = "epsilon",
            thresholding: bool = False,
            dynamic_thresholding_ratio: float = 0.995,
            sample_max_value: float = 1.0,
            predict_x0: bool = True,
            solver_type: str = "bh2",
            lower_order_final: bool = True,
            disable_corrector: List[int] = [],
            solver_p: SchedulerMixin = None,
            use_karras_sigmas: Optional[bool] = False,
            timestep_spacing: str = "linspace",
            steps_offset: int = 0,
        )
        '''
        output = UniPCMultistepScheduler.from_config(config)
    else:
        '''
        config = dict(
            num_train_timesteps = 1000,
            beta_start = 0.00085,  # sensible defaults
            beta_end = 0.012,
            beta_schedule = 'linear',
            trained_betas = None,
            prediction_type = 'epsilon',
            use_karras_sigmas = False,
            noise_sampler_seed = None,
            timestep_spacing = 'linspace',
            steps_offset = 0,
        )
        '''
        output = DPMSolverSDEScheduler.from_config(config)
    return output


'''
@app.post('/v0/render/lora/zero')
async def v0_render_lora_zero_handle(req: RenderRequest):
    size = req.size.split(',')
    seed = req.seed if req.seed > 0 else random.randint(1, 2**64)
    engine = f'sd15-{req.engine}.safetensors' if '.' not in req.engine else req.engine
     = f'sd15-subj-{req.subject}.safetensors' if '.' not in req.subject else req.subject
    sampler = 'euler_ancestral' if req.sampler == 'euler_a' else req.sampler
    positive = f'{req.positive}, ({req.hero})'

    image = v0_render_lora_single(
        session,
        uuid=req.uuid,
        size=(int(size[0]), int(size[1])),
        steps=req.steps,
        scale=req.scale,
        seed=seed,
        sampler=sampler,
        engine=engine,
        subject=subject,
        positive=positive,
        negative=req.negative
    )
    name = f'{req.subject}-{req.engine}-{req.uuid}-{seed}.png'
    dst_fil = list_files(dst_dir, 'png')[-1][1]
    dst_img = base64.b64encode(open(dst_fil, 'rb').read()).decode('utf-8')
    return {name: dst_img}


@app.post('/render')
@app.post('/v0/render/lora/single')
async def v0_render_lora_single_handle(req: RenderRequest):
    size = req.size.split(',')
    seed = req.seed if req.seed > 0 else random.randint(1, 2**64)
    engine = f'sd15-{req.engine}.safetensors' if '.' not in req.engine else req.engine
    subject = f'sd15-subj-{req.subject}.safetensors' if '.' not in req.subject else req.subject
    sampler = 'euler_ancestral' if req.sampler == 'euler_a' else req.sampler

    image = v0_render_lora_single(
        session,
        uuid=req.uuid,
        size=(int(size[0]), int(size[1])),
        steps=req.steps,
        scale=req.scale,
        seed=seed,
        sampler=sampler,
        engine=engine,
        subject=subject,
        positive=req.positive,
        negative=req.negative
    )
    name = f'{req.subject}-{req.engine}-{req.uuid}-{seed}.png'
    dst_fil = list_files(dst_dir, 'png')[-1][1]
    dst_img = base64.b64encode(open(dst_fil, 'rb').read()).decode('utf-8')
    return {name: dst_img}


@app.post('/v0/render/lora/double')
async def v0_render_lora_double_handle(req: RenderRequest):
    size = req.size.split(',')
    seed = req.seed if req.seed > 0 else random.randint(1, 2**64)
    engine = f'sd15-{req.engine}.safetensors' if '.' not in req.engine else req.engine
    subject = f'sd15-subj-{req.subject}.safetensors' if '.' not in req.subject else req.subject
    sampler = 'euler_ancestral' if req.sampler == 'euler_a' else req.sampler

    image = v0_render_lora_double(
        session,
        uuid=req.uuid,
        size=(int(size[0]), int(size[1])),
        steps=req.steps,
        scale=req.scale,
        seed=seed,
        sampler=sampler,
        engine=engine,
        subject=subject,
        positive=req.positive,
        negative=req.negative
    )
    name = f'{req.subject}-{req.engine}-{req.uuid}-{seed}.png'
    return {name: image}


@app.post('/v1/render/lora/zero')
async def v1_render_lora_zero_handle(req: RenderRequest):
    size = req.size.split(',')
    seed = req.seed if req.seed > 0 else random.randint(1, 2**64)
    engine = f'sd15-{req.engine}.safetensors' if '.' not in req.engine else req.engine
    subject = f'sd15-subj-{req.subject}.safetensors' if '.' not in req.subject else req.subject
    sampler = 'euler_ancestral' if req.sampler == 'euler_a' else req.sampler
    positive = f'{req.positive}, ({req.hero})'

    image = v1_render_lora_zero(
        session,
        uuid=req.uuid,
        size=(int(size[0]), int(size[1])),
        steps=req.steps,
        scale=req.scale,
        seed=seed,
        sampler=sampler,
        engine=engine,
        positive=positive,
        negative=req.negative
    )
    name = f'{req.subject}-{req.engine}-{req.uuid}-{seed}.png'
    return {name: image}



@app.post('/v1/render/lora/double')
async def v1_render_lora_double_handle(req: RenderRequest):
    size = req.size.split(',')
    seed = req.seed if req.seed > 0 else random.randint(1, 2**64)
    engine = f'sd15-{req.engine}.safetensors' if '.' not in req.engine else req.engine
    subject = f'sd15-subj-{req.subject}.safetensors' if '.' not in req.subject else req.subject
    sampler = 'euler_ancestral' if req.sampler == 'euler_a' else req.sampler

    image = v1_render_lora_double(
        session,
        uuid=req.uuid,
        size=(int(size[0]), int(size[1])),
        steps=req.steps,
        scale=req.scale,
        seed=seed,
        sampler=sampler,
        engine=engine,
        subject=subject,
        positive=req.positive,
        negative=req.negative
    )
    name = f'{req.subject}-{req.engine}-{req.uuid}-{seed}.png'
    return {name: image}
'''
