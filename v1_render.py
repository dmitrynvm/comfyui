import os
import torch
import shutil
import warnings
import os.path as osp
from PIL import Image
from diffusers import StableDiffusionPipeline
from utility import (
    get_sampler,
    load_image,
    save_image
)
warnings.filterwarnings('ignore')


def v1_render(
    session,
    uuid,
    size,
    steps,
    scale,
    seed,
    sampler,
    engine,
    adapter,
    positive,
    negative
):
    dst_dir='output'
    os.makedirs(dst_dir, exist_ok=True)
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    save_path = osp.join(dst_dir, f'{session}_render.png')
    pipe = StableDiffusionPipeline.from_single_file(engine, torch_dtype=torch.float16).to('cuda')
    '''
    if subject:
        pipe.load_lora_weights(subject)
        pipe.fuse_lora()
    if style:
        pipe.load_lora_weights(style)
        pipe.fuse_lora()
    '''
    pipe.load_lora_weights(adapter[0])
    #pipe.load_lora_weights(addons[0], adapter_name='subject')
    #pipe.load_lora_weights(style, adapter_name='style')
    #pipe.set_adapters(['subject', 'style'], adapter_weights=[0.7, 1.0])

    pipe.scheduler = get_sampler(sampler)
    img = pipe(
        prompt=positive,
        negative=negative,
        generator=torch.Generator(device).manual_seed(seed),
        num_inference_steps=steps,
        guidance_scale=scale,
        width=size[0],
        height=size[1]
    ).images[0]
    if osp.exists(dst_dir):
        os.makedirs(dst_dir, exist_ok=True)
    img.save(save_path)
    return load_image(save_path)


if __name__ == '__main__':
    v1_render(
        session='pdagjn4',
        uuid=2319401,
        size=(512, 512),
        steps=30,
        scale=7,
        seed=0,
        sampler='euler',
        engine='models/checkpoints/sd15-photon.safetensors',
        adapter=['models/loras/sd15-lora-mrbean.safetensors', 'models/loras/vg15-lora-vectorize.safetensors'],
        positive='masterpiece, best quality), Arnold Schwarzenegger (as rastaman wearing rasta beanie), 1 man, epic (photo, studio lighting, hard light, sony a7, 50 mm, matte skin, pores, colors, hyperdetailed, hyperrealistic), ultrasharp,  <lora:swarz:0.95>',
        negative='(nsfw:1.3), (nude:1.3), text, black and white, illustration, painting, cartoon, 3d, bad art, poorly drawn, close up, blurry, disfigured, deformed, extra limbs'
    )
