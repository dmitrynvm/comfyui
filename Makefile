gui:
	python main.py

run:
	uvicorn run:app --port 8000

test:
	python -m pytest tests/


clean:
	rm -rf logs
	rm -rf temp
	find . | grep -E "(__pycache__|\.pyc|\.pyo)" | xargs rm -rf
	find . | grep -E ".pytest_cache" | xargs rm -rf

