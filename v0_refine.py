import os
import random
from typing import Sequence, Mapping, Any, Union
import torch


def get_value_at_index(obj: Union[Sequence, Mapping], index: int) -> Any:
    try:
        return obj[index]
    except KeyError:
        return obj['result'][index]


def import_custom_nodes() -> None:
    import asyncio
    import execution
    from nodes import init_custom_nodes
    import server
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    server_instance = server.PromptServer(loop)
    execution.PromptQueue(server_instance)
    init_custom_nodes()


from nodes import (
    LoadImage,
    SaveImage,
    CheckpointLoaderSimple,
    KSampler,
    CLIPTextEncode,
    VAEEncode,
    VAEDecode,
    SetLatentNoiseMask,
    NODE_CLASS_MAPPINGS,
)


def v0_refine(
    session,
    original,
    reference,
    engine,
    positive,
    negative
):
    import_custom_nodes()
    with torch.inference_mode():
        checkpointloadersimple = CheckpointLoaderSimple()
        checkpointloadersimple_4 = checkpointloadersimple.load_checkpoint(
            ckpt_name=engine
        )

        cliptextencode = CLIPTextEncode()
        cliptextencode_7 = cliptextencode.encode(
            text=negative,
            clip=get_value_at_index(checkpointloadersimple_4, 1),
        )

        cliptextencode_208 = cliptextencode.encode(
            text=positive,
            clip=get_value_at_index(checkpointloadersimple_4, 1),
        )

        loadimage = LoadImage()
        loadimage_209 = loadimage.load_image(image=original)

        vaeencode = VAEEncode()
        vaeencode_210 = vaeencode.encode(
            pixels=get_value_at_index(loadimage_209, 0),
            vae=get_value_at_index(checkpointloadersimple_4, 2),
        )

        loadimage_213 = loadimage.load_image(image=reference)

        facerestoremodelloader = NODE_CLASS_MAPPINGS["FaceRestoreModelLoader"]()
        facerestoremodelloader_219 = facerestoremodelloader.load_model(
            model_name="gfpgan.pth"
        )

        upscalemodelloader = NODE_CLASS_MAPPINGS["UpscaleModelLoader"]()
        upscalemodelloader_222 = upscalemodelloader.load_model(
            model_name="4x-realesrgan.pth"
        )

        upscalemodelloader_226 = upscalemodelloader.load_model(
            model_name="1x-skindetail.pth"
        )

        clipseg = NODE_CLASS_MAPPINGS["CLIPSeg"]()
        setlatentnoisemask = SetLatentNoiseMask()
        ksampler = KSampler()
        vaedecode = VAEDecode()
        reactorfaceswap = NODE_CLASS_MAPPINGS["ReActorFaceSwap"]()
        facerestorecfwithmodel = NODE_CLASS_MAPPINGS["FaceRestoreCFWithModel"]()
        imageupscalewithmodel = NODE_CLASS_MAPPINGS["ImageUpscaleWithModel"]()
        saveimage = SaveImage()

        for q in range(1):
            clipseg_206 = clipseg.segment_image(
                text="man",
                blur=4,
                threshold=0.4,
                dilation_factor=1,
                image=get_value_at_index(loadimage_209, 0),
            )

            setlatentnoisemask_205 = setlatentnoisemask.set_mask(
                samples=get_value_at_index(vaeencode_210, 0),
                mask=get_value_at_index(clipseg_206, 0),
            )

            ksampler_196 = ksampler.sample(
                seed=random.randint(1, 2**64),
                steps=20,
                cfg=8,
                sampler_name="euler",
                scheduler="normal",
                denoise=0.9,
                model=get_value_at_index(checkpointloadersimple_4, 0),
                positive=get_value_at_index(cliptextencode_208, 0),
                negative=get_value_at_index(cliptextencode_7, 0),
                latent_image=get_value_at_index(setlatentnoisemask_205, 0),
            )

            vaedecode_204 = vaedecode.decode(
                samples=get_value_at_index(ksampler_196, 0),
                vae=get_value_at_index(checkpointloadersimple_4, 2),
            )

            reactorfaceswap_212 = reactorfaceswap.execute(
                enabled=True,
                swap_model="inswapper_128.onnx",
                facedetection="retinaface_resnet50",
                face_restore_model="none",
                face_restore_visibility=1,
                codeformer_weight=0.5,
                detect_gender_source="no",
                detect_gender_input="no",
                source_faces_index="0",
                input_faces_index="0",
                console_log_level=1,
                input_image=get_value_at_index(vaedecode_204, 0),
                source_image=get_value_at_index(loadimage_213, 0),
            )

            facerestorecfwithmodel_218 = facerestorecfwithmodel.restore_face(
                facedetection="retinaface_resnet50",
                codeformer_fidelity=0.5,
                facerestore_model=get_value_at_index(facerestoremodelloader_219, 0),
                image=get_value_at_index(reactorfaceswap_212, 0),
            )

            imageupscalewithmodel_224 = imageupscalewithmodel.upscale(
                upscale_model=get_value_at_index(upscalemodelloader_222, 0),
                image=get_value_at_index(facerestorecfwithmodel_218, 0),
            )

            imageupscalewithmodel_227 = imageupscalewithmodel.upscale(
                upscale_model=get_value_at_index(upscalemodelloader_226, 0),
                image=get_value_at_index(imageupscalewithmodel_224, 0),
            )

            saveimage_225 = saveimage.save_images(
                filename_prefix=f"{session}_refine",
                images=get_value_at_index(imageupscalewithmodel_227, 0),
            )


if __name__ == '__main__':
    v0_refine(
        session='pdagjn4',
        original='original.png',
        reference='reference.png',
        engine='sd15-absreal-inpaint.safetensors',
        positive='a man on the same background, panorama light',
        negative='watermark, blur, wrinkles, open mouth'
    )
