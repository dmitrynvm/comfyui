import os
import random
import sys
from typing import Sequence, Mapping, Any, Union
import torch


def get_value_at_index(obj: Union[Sequence, Mapping], index: int) -> Any:
    try:
        return obj[index]
    except KeyError:
        return obj["result"][index]


def import_custom_nodes() -> None:
    import asyncio
    import execution
    from nodes import init_custom_nodes
    import server
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    server_instance = server.PromptServer(loop)
    execution.PromptQueue(server_instance)
    init_custom_nodes()


from nodes import (
    SaveImage,
    NODE_CLASS_MAPPINGS
)


def generate(
    name,
    size,
    steps,
    scale,
    seed,
    sampler,
    model,
    lora,
    positive_prompt,
    negative_prompt
):
    import_custom_nodes()
    with torch.inference_mode():
        upscalemodelloader = NODE_CLASS_MAPPINGS["UpscaleModelLoader"]()
        upscalemodelloader_14 = upscalemodelloader.load_model(
            model_name="4x-realesrgan.pth"
        )
        upscalemodelloader_16 = upscalemodelloader.load_model(
            model_name="1x-skindetail.pth"
        )
        efficient_loader = NODE_CLASS_MAPPINGS["Efficient Loader"]()
        ksampler_efficient = NODE_CLASS_MAPPINGS["KSampler (Efficient)"]()
        imageupscalewithmodel = NODE_CLASS_MAPPINGS["ImageUpscaleWithModel"]()
        saveimage = SaveImage()

        for q in range(1):
            efficient_loader_1 = efficient_loader.efficientloader(
                ckpt_name=model,
                vae_name="Baked VAE",
                clip_skip=-1,
                lora_name=None,
                lora_model_strength=1,
                lora_clip_strength=1,
                positive=positive_prompt,
                negative=negative_prompt,
                token_normalization="none",
                weight_interpretation="comfy",
                empty_latent_width=size[0],
                empty_latent_height=size[1],
                batch_size=1,
            )

            ksampler_efficient_2 = ksampler_efficient.sample(
                seed=random.randint(1, 2**64),
                steps=steps,
                cfg=scale,
                sampler_name=sampler,
                scheduler="normal",
                denoise=1,
                preview_method="auto",
                vae_decode="true",
                model=get_value_at_index(efficient_loader_1, 0),
                positive=get_value_at_index(efficient_loader_1, 1),
                negative=get_value_at_index(efficient_loader_1, 2),
                latent_image=get_value_at_index(efficient_loader_1, 3),
                optional_vae=get_value_at_index(efficient_loader_1, 4),
            )

            imageupscalewithmodel_15 = imageupscalewithmodel.upscale(
                upscale_model=get_value_at_index(upscalemodelloader_14, 0),
                image=get_value_at_index(ksampler_efficient_2, 5),
            )

            imageupscalewithmodel_17 = imageupscalewithmodel.upscale(
                upscale_model=get_value_at_index(upscalemodelloader_16, 0),
                image=get_value_at_index(imageupscalewithmodel_15, 0),
            )

            saveimage_18 = saveimage.save_images(
                filename_prefix=name,
                images=get_value_at_index(imageupscalewithmodel_17, 0),
            )


if __name__ == "__main__":
    generate(
        name='norris_lyriel',
        size=(512, 512),
        steps=30,
        scale=7,
        seed=-1,
        sampler='dpmpp_2m',
        model='sd15-lyriel.safetensors',
        lora='sd15-subj-norris.safetensors',
        positive_prompt='A stunning intricate full color portrait of (sks person:1), <lora:locon_chuck_v1_from_v1_64_32:1.3>, in s1l0A1 style',
        negative_prompt='cartoon, 3d, ((deformed)), ((poorly drawn)), weird colors, blurry'
    )
