import argparse
import itertools
import os
import sys
import shutil
import os.path as osp
from pathlib import Path
from huggingface_hub import hf_hub_download
from subprocess import run
from fire import Fire
from logzero import logger


def download(src, dst, inp, out):
    dst_out = osp.join(dst, out)
    if osp.exists(dst_out):
        logger.info(f'The file {out} is already downloaded.')
        return
    hf_hub_download(
        repo_id=src,
        local_dir=dst,
        filename=inp,
        local_dir_use_symlinks=False
    )
    #Path(osp.join(dst, inp)).rename(osp.join(dst, out))
    #run(['sed', '-i', ";", osp.join(dst, out)])
    #shutil.rmtree(osp.expanduser('~/.cache/huggingface/hub/'))
    logger.info(f'The file {out} was downloaded successfully.')
    return


if __name__ == '__main__':
    Fire(download)
