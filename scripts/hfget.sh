#python hfget.py --src runwayml/stable-diffusion-v1-5 --inp v1-5-pruned.safetensors --dst models/checkpoints --out sd15-pruned.safetensors
#python hfget.py --src stabilityai/stable-diffusion-xl-base-1.0 --inp sd_xl_base_1.0.safetensors --dst models/checkpoints --out sdxl-base.safetensors
#python hfget.py --src stabilityai/stable-diffusion-2-depth --inp 512-depth-ema.safetensors --dst models/checkpoints --out sdxl-depth.safetensors
#python hfget.py --src stabilityai/stable-diffusion-2-1 --inp v2-1_768-ema-pruned.safetensors --dst models/checkpoints --out sd21-pruned.safetensors
#python hfget.py --src runwayml/stable-diffusion-v1-5 --inp v1-5-pruned-emaonly.safetensors --dst models/checkpoints --out sd15-pruned-emaonly.safetensors
#python hfget.py --src diffusers/controlnet-depth-sdxl-1.0 --inp diffusion_pytorch_model.safetensors --dst models/checkpoints --out cnxl-depth.safetensors
python hfget.py --src eqawqw/absolutereality_v181INPAINTING --inp absolutereality_v181INPAINTING.safetensors --dst models/checkpoints --out absreal-inpaint.safetensors
