python hfget.py --src h94/IP-Adapter --inp models/ip-adapter-full-face_sd15.safetensors --dst models/checkpoints --out ip-adapter-full-face_sd15.safetensors
python hfget.py --src h94/IP-Adapter --inp models/ip-adapter-plus-face_sd15.safetensors --dst models/checkpoints --out ip-adapter-plus-face_sd15.safetensors
python hfget.py --src h94/IP-Adapter --inp models/ip-adapter-plus_sd15.safetensors --dst models/checkpoints --out ip-adapter-plus_sd15.safetensors
python hfget.py --src h94/IP-Adapter --inp models/ip-adapter_sd15.safetensors --dst models/checkpoints --out ip-adapter_sd15.safetensors
python hfget.py --src h94/IP-Adapter --inp models/ip-adapter_sd15_light.safetensors --dst models/checkpoints --out ip-adapter_sd15_light.safetensors
python hfget.py --src h94/IP-Adapter --inp models/ip-adapter_sd15_vit-G.safetensors --dst models/checkpoints --out ip-adapter_sd15_vit-G.safetensors
