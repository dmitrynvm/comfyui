import base64
import io
import os
import os.path as osp
import random
import shortuuid
import shutil
from fastapi import FastAPI, Request, Response
from pydantic import BaseModel
from PIL import Image
from v0_refine import v0_refine
from v0_render import v0_render
from v1_render import v1_render
from utility import (
    get_ckpt,
    get_ckpts,
    get_files,
    get_sampler,
    get_size,
    get_seed,
    save_image,
    load_image
)


app = FastAPI()
session = shortuuid.ShortUUID().random(length=22)
src_dir = f'input'
dst_dir = f'output'
original_fil = f'{session}_original.png'
reference_fil = f'{session}_reference.png'
os.makedirs(src_dir, mode=0o777, exist_ok=True)
os.makedirs(dst_dir, mode=0o777, exist_ok=True)


class RefineRequest(BaseModel):
    original: str
    reference: str
    engine: str | None = 'sd15-absreal-inpaint'
    positive: str | None = 'a man on the same background, panorama light'
    negative: str | None = 'watermark, blur, wrinkles, open mouth'


class RenderRequest(BaseModel):
    concept: str | None = ''
    uuid: int
    size: str
    steps: int
    scale: int
    seed: int
    sampler: str
    engine: str
    adapter: str | None = ''
    positive: str | None = ''
    negative: str | None = ''


@app.post('/refine')
@app.post('/v0/refine')
async def v0_refine_handle(req: RefineRequest):
    engine = get_ckpt(req.engine, short=True)
    src_original = osp.join(src_dir, original_fil)
    src_reference = osp.join(src_dir, reference_fil)
    original_bytes = base64.b64decode(req.original)
    reference_bytes = base64.b64decode(req.reference)
    original_img = Image.open(io.BytesIO(original_bytes))
    reference_img = Image.open(io.BytesIO(reference_bytes))
    original_img.save(src_original)
    reference_img.save(src_reference)
    v0_refine(
        session,
        original=original_fil,
        reference=reference_fil,
        engine=engine,
        positive=req.positive,
        negative=req.negative
    )
    dst_refined = get_files(dst_dir, session, 'png')[-1][1]
    refined = open(dst_refined, 'rb').read()
    image = base64.b64encode(refined).decode('utf-8')
    return {'image': image}


@app.post('/v0/render')
async def v1_render_handle(req: RenderRequest):
    seed = get_seed(req.seed)
    size = get_size(req.size)
    sampler = get_sampler(req.sampler)
    engine = get_ckpt(req.engine, short=True)
    adapter = get_ckpts(req.adapter, short=True)

    image = v0_render(
        session,
        req.uuid,
        size,
        req.steps,
        req.scale,
        seed,
        sampler,
        engine,
        adapter,
        req.positive,
        req.negative
    )
    a = '-'.join([a.split('-')[2] for a in adapter])
    e = engine.split('-')[1]
    name = f'{a}-{e}-{req.uuid}-{seed}.png'
    return {name: image}



@app.post('/render')
@app.post('/v1/render')
async def v1_render_handle(req: RenderRequest):
    seed = get_seed(req.seed)
    size = get_size(req.size)
    sampler = get_sampler(req.sampler)
    engine = get_ckpt(req.engine)
    adapter = get_ckpts(req.adapter)
    image = v1_render(
        session,
        req.uuid,
        size,
        req.steps,
        req.scale,
        seed,
        sampler,
        engine,
        adapter,
        req.positive,
        req.negative
    )
    a = '-'.join([a.split('-')[2] for a in adapter])
    e = engine.split('-')[1]
    name = f'{a}-{e}-{req.uuid}-{seed}.png'
    return {name: image}


if __name__ == '__main__':
    pass
