# Manual

1. Activate virtual environment
```bash
source venv/bin/activate
```

2. Run the tests 
```bash
make test
```

3. Run the entrypoint 
```bash
make run
```

4. Post images on http://127.0.0.1:8000/refine and get their refined versions in output folder
```bash
cd output
ls
```

5. Clean-up the environment
```bash
make clean
```

