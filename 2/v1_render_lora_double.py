import os
import random
import sys
from typing import Sequence, Mapping, Any, Union
import torch


def get_value_at_index(obj: Union[Sequence, Mapping], index: int) -> Any:
    try:
        return obj[index]
    except KeyError:
        return obj["result"][index]


def find_path(name: str, path: str = None) -> str:
    if path is None:
        path = os.getcwd()
    if name in os.listdir(path):
        path_name = os.path.join(path, name)
        print(f"{name} found: {path_name}")
        return path_name
    parent_directory = os.path.dirname(path)
    if parent_directory == path:
        return None
    return find_path(name, parent_directory)


def import_custom_nodes() -> None:
    import asyncio
    import execution
    from nodes import init_custom_nodes
    import server
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    server_instance = server.PromptServer(loop)
    execution.PromptQueue(server_instance)
    init_custom_nodes()


from nodes import (
    CLIPTextEncode,
    KSampler,
    EmptyLatentImage,
    CheckpointLoaderSimple,
    SaveImage,
    NODE_CLASS_MAPPINGS,
    LoraLoader,
    VAEDecode,
)


def v1_render_lora_double(
    session,
    uuid,
    size,
    steps,
    scale,
    seed,
    sampler,
    engine,
    subject,
    style,
    positive,
    negative,
):
    import_custom_nodes()
    with torch.inference_mode():
        checkpointloadersimple = CheckpointLoaderSimple()
        checkpointloadersimple_4 = checkpointloadersimple.load_checkpoint(
            ckpt_name=engine
        )

        emptylatentimage = EmptyLatentImage()
        emptylatentimage_5 = emptylatentimage.generate(
            width=size[0], height=size[1], batch_size=1
        )

        loraloader = LoraLoader()
        loraloader_14 = loraloader.load_lora(
            lora_name=subject,
            strength_model=1,
            strength_clip=1,
            model=get_value_at_index(checkpointloadersimple_4, 0),
            clip=get_value_at_index(checkpointloadersimple_4, 1),
        )

        loraloader_16 = loraloader.load_lora(
            lora_name=style,
            strength_model=1,
            strength_clip=1,
            model=get_value_at_index(loraloader_14, 0),
            clip=get_value_at_index(loraloader_14, 1),
        )

        cliptextencode = CLIPTextEncode()
        cliptextencode_6 = cliptextencode.encode(
            text=positive,
            clip=get_value_at_index(loraloader_16, 1),
        )

        cliptextencode_7 = cliptextencode.encode(
            text=negative,
            clip=get_value_at_index(loraloader_16, 1),
        )

        ksampler = KSampler()
        vaedecode = VAEDecode()
        saveimage = SaveImage()

        ksampler_3 = ksampler.sample(
            seed=seed,
            steps=steps,
            cfg=scale,
            sampler_name=sampler,
            scheduler="normal",
            denoise=1,
            model=get_value_at_index(loraloader_16, 0),
            positive=get_value_at_index(cliptextencode_6, 0),
            negative=get_value_at_index(cliptextencode_7, 0),
            latent_image=get_value_at_index(emptylatentimage_5, 0),
        )

        vaedecode_8 = vaedecode.decode(
            samples=get_value_at_index(ksampler_3, 0),
            vae=get_value_at_index(checkpointloadersimple_4, 2),
        )

        saveimage_9 = saveimage.save_images(
            filename_prefix=session,
            images=get_value_at_index(vaedecode_8, 0),
        )


if __name__ == "__main__":
    v1_render_lora_double(
        session='pdagjn4',
        uuid=1294848,
        size=(512,512),
        steps=22,
        scale=7,
        seed=222222,
        sampler='dpmpp_2m',
        engine='sd15-jernau.safetensors',
        subject='sd15-subj-mrbean.safetensors',
        style='sd15-styl-absreal.safetensors',
        positive='masterpiece, best quality), Arnold Schwarzenegger (as rastaman wearing rasta beanie),  1 man, epic (photo, studio lighting, hard light, sony a7, 50 mm, matte skin, pores, colors, hyperdetailed, hyperrealistic), ultrasharp,  <lora:swarz:0.95>',
        negative='(nsfw:1.3), (nude:1.3), text, black and white, illustration, painting, cartoon, 3d, bad art, poorly drawn, close up, blurry, disfigured, deformed, extra limbs'
    )
