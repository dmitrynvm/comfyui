import base64
import io
import os
import torch
import shutil
import warnings
import os.path as osp
from PIL import Image
from diffusers import (
    StableDiffusionPipeline,
    EulerDiscreteScheduler,
    EulerAncestralDiscreteScheduler,
    DPMSolverSinglestepScheduler,
    DPMSolverMultistepScheduler,
    DPMSolverSDEScheduler,
    UniPCMultistepScheduler
)
warnings.filterwarnings('ignore')


config = dict(
    num_train_timesteps=1000,
    beta_start=0.00085,
    beta_end=0.012,
    beta_schedule='scaled_linear',
    trained_betas=None,
    clip_sample=False,
    set_alpha_to_one=False,
    steps_offset=1,
    prediction_type='epsilon',
    thresholding=False,
    dynamic_thresholding_ratio = 0.995,
    clip_sample_range = 1.0,
    sample_max_value = 1.0,
    timestep_spacing = 'leading',
    rescale_betas_zero_snr = False,
    interpolation_type = 'linear',
    skip_prk_steps = True
)


def load_image(fil):
    return base64.b64encode(open(fil, 'rb').read()).decode('utf-8')


def save_image(fil, txt):
    io.BytesIO(base64.b64decode(txt)).save(fil)


def create_sampler(sampler):
    if sampler == 'euler':
        '''
        config = dict(
            beta_end = 0.012,
            beta_schedule = 'scaled_linear',
            beta_start = 0.00085,
            clip_sample = False,
            clip_sample_range = 1.0,
            dynamic_thresholding_ratio = 0.995,
            interpolation_type = 'linear',
            num_train_timesteps = 1000,
            prediction_type = 'epsilon',
            rescale_betas_zero_snr = False,
            sample_max_value = 1.0,
            set_alpha_to_one = False,
            sigma_max = None,
            sigma_min = None,
            skip_prk_steps = True,
            steps_offset = 1,
            thresholding = False,
            timestep_spacing = 'linspace',
            timestep_type = 'discrete',
            trained_betas = None,
            use_karras_sigmas = False
        )
        '''
        output = EulerDiscreteScheduler.from_config(config)
    elif sampler == 'euler_ancestral':
        '''
        config = dict(
            num_train_timesteps = 1000,
            beta_start = 0.0001,
            beta_end = 0.02,
            beta_schedule = 'linear',
            trained_betas = None,
            prediction_type = 'epsilon',
            timestep_spacing = 'linspace',
            steps_offset = 0,
            rescale_betas_zero_snr = False
        )
        config = {
            'num_train_timesteps': 1000,
            'beta_start': 0.00085,
            'beta_end': 0.012,
            'beta_schedule': 'scaled_linear',
            'trained_betas': None,
            'clip_sample': False,
            'set_alpha_to_one': False,
            'steps_offset': 1,
            'prediction_type': 'epsilon',
            'thresholding': False,
            'dynamic_thresholding_ratio': 0.995,
            'clip_sample_range': 1.0,
            'sample_max_value': 1.0,
            'timestep_spacing': 'leading',
            'rescale_betas_zero_snr': False,
            'interpolation_type': 'linear',
            'skip_prk_steps': True
        }
        '''
        output = EulerAncestralDiscreteScheduler.from_config(config)
    elif sampler == 'dpm_2':
        '''
        config = dict(
            num_train_timesteps = 1000,
            beta_start = 0.0001,
            beta_end = 0.02,
            beta_schedule = 'linear',
            trained_betas = None,
            solver_order = 2,
            prediction_type = 'epsilon',
            thresholding = False,
            dynamic_thresholding_ratio = 0.995,
            sample_max_value = 1.0,
            algorithm_type = 'dpmsolver++',
            solver_type = 'midpoint',
            lower_order_final = False,
            use_karras_sigmas = False,
            final_sigmas_type = 'zero',
            lambda_min_clipped = -float('inf'),
            variance_type = None
        )
        '''
        output = DPMSolverSinglestepScheduler.from_config(config)
    elif sampler == 'dpmpp_2m':
        '''
        config = dict(
            num_train_timesteps = 1000,
            beta_start = 0.0001,
            beta_end = 0.02,
            beta_schedule = 'linear',
            trained_betas = None,
            solver_order = 2,
            prediction_type = 'epsilon',
            thresholding = False,
            dynamic_thresholding_ratio = 0.995,
            sample_max_value = 1.0,
            algorithm_type = 'dpmsolver++',
            solver_type = 'midpoint',
            lower_order_final = True,
            euler_at_final = False,
            use_karras_sigmas = False,
            use_lu_lambdas = False,
            final_sigmas_type = 'zero',  # 'zero', 'sigma_min'
            lambda_min_clipped = -float('inf'),
            variance_type = None,
            timestep_spacing = 'linspace',
            steps_offset = 0,
        )
        '''
        output = DPMSolverMultistepScheduler.from_config(config)
    elif sampler == 'unipc':
        '''
        config = dict(
            num_train_timesteps: int = 1000,
            beta_start: float = 0.0001,
            beta_end: float = 0.02,
            beta_schedule: str = "linear",
            trained_betas: Optional[Union[np.ndarray, List[float]]] = None,
            solver_order: int = 2,
            prediction_type: str = "epsilon",
            thresholding: bool = False,
            dynamic_thresholding_ratio: float = 0.995,
            sample_max_value: float = 1.0,
            predict_x0: bool = True,
            solver_type: str = "bh2",
            lower_order_final: bool = True,
            disable_corrector: List[int] = [],
            solver_p: SchedulerMixin = None,
            use_karras_sigmas: Optional[bool] = False,
            timestep_spacing: str = "linspace",
            steps_offset: int = 0,
        )
        '''
        output = UniPCMultistepScheduler.from_config(config)
    else:
        '''
        config = dict(
            num_train_timesteps = 1000,
            beta_start = 0.00085,  # sensible defaults
            beta_end = 0.012,
            beta_schedule = 'linear',
            trained_betas = None,
            prediction_type = 'epsilon',
            use_karras_sigmas = False,
            noise_sampler_seed = None,
            timestep_spacing = 'linspace',
            steps_offset = 0,
        )
        '''
        output = DPMSolverSDEScheduler.from_config(config)
    return output


def v2_render_lora_double(
    session,
    uuid,
    size,
    steps,
    scale,
    seed,
    sampler,
    engine,
    subject,
    style,
    positive,
    negative,
    dst_dir='output',
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
):
    os.makedirs(dst_dir, exist_ok=True)
    save_path = osp.join(dst_dir, f'{session}_render.png')
    ckpt_path = osp.join('models', 'checkpoints', engine)
    pipe = StableDiffusionPipeline.from_single_file(ckpt_path, torch_dtype=torch.float16).to('cuda')
    subject_path = osp.join('models', 'loras', subject)
    style_path = osp.join('models', 'loras', style)
    pipe.load_lora_weights(subject_path, adapter_name='subject')
    pipe.load_lora_weights(style_path, adapter_name='style')
    pipe.set_adapters(['subject', 'style'], adapter_weights=[0.7, 1.0])
    #pipe.set_adapters(['style', 'subject'], adapter_weights=[1.0, 0.7])

    pipe.scheduler = create_sampler(sampler)
    img = pipe(
        prompt=positive,
        negative=negative,
        generator = torch.Generator(device).manual_seed(seed),
        num_inference_steps = steps,
        guidance_scale = scale,
        width = size[0],
        height = size[1]
    ).images[0]
    if osp.exists(dst_dir):
        os.makedirs(dst_dir, exist_ok=True)
    img.save(save_path)
    return load_image(save_path)


if __name__ == '__main__':
    v2_render_lora_double(
        session='pdagjn4',
        uuid=2319401,
        size=(512, 512),
        steps=30,
        scale=7,
        seed=0,
        sampler='euler',
        engine='sd15-epicreal.safetensors',
        subject='sd15-subj-arnold.safetensors',
        style='sd15-styl-absreal.safetensors',
        positive='masterpiece, best quality), Arnold Schwarzenegger (as rastaman wearing rasta beanie),  1 man, epic (photo, studio lighting, hard light, sony a7, 50 mm, matte skin, pores, colors, hyperdetailed, hyperrealistic), ultrasharp,  <lora:swarz:0.95>',
        negative='(nsfw:1.3), (nude:1.3), text, black and white, illustration, painting, cartoon, 3d, bad art, poorly drawn, close up, blurry, disfigured, deformed, extra limbs'
    )
