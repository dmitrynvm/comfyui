

def test_render(http_client, v0_render_url, headers):
    data = {
        'concept': 'Arnold Shwarzenegger',
        'uuid': '2595612',
        'size': '512,512',
        'steps': 35,
        'scale': 7,
        'seed': 154775472,
        'sampler': 'dpmpp_sde',
        'engine': 'sd15-slicker',
        'adapter': 'sd15-lora-arnold',
        'positive': '(masterpiece, best quality), Arnold Schwarzenegger (The Expendables movie), 1 man, epic (photo, studio lighting, hard light, sony a7, 50 mm, matte skin, pores, colors, hyperdetailed, hyperrealistic), ultrasharp,  <subject:swarz:0.95>',
        'negative': '(nsfw:1.3), (nude:1.3), text, black and white, illustration, painting, cartoon, 3d, bad art, poorly drawn, close up, blurry, disfigured, deformed, extra limbs'
    }
    res = http_client.post(v0_render_url, headers=headers, json=data)
    assert res.status_code == 200
