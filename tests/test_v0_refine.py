

def test_refine(http_client, v0_refine_url, headers, original, reference):
    data = {
        'original': original,
        'reference': reference
    }
    res = http_client.post(v0_refine_url, headers=headers, json=data)
    assert res.status_code == 200
