import base64
import pytest
import os.path as osp
from fastapi.testclient import TestClient
from run import app


client = TestClient(app)
host = '127.0.0.1'
port = 8000
src_dir = 'assets'
original_fil = 'original.png'
reference_fil = 'reference.png'
src_original = osp.join(src_dir, original_fil)
src_reference = osp.join(src_dir, reference_fil)


@pytest.fixture(scope='package')
def original():
    original_img = open(src_original, 'rb').read()
    return base64.b64encode(original_img).decode('utf-8')


@pytest.fixture(scope='package')
def reference():
    reference_img = open(src_reference, 'rb').read()
    return base64.b64encode(reference_img).decode('utf-8')


@pytest.fixture(scope='package')
def headers():
    return {
        'charset': 'utf-8', 
        'content-type': 'application/json'
    }


@pytest.fixture(scope='package')
def http_client():
    return client


@pytest.fixture(scope='package')
def url():
    return f'http://{host}:{port}'


@pytest.fixture(scope='package')
def refine_url():
    return f'http://{host}:{port}/refine'


@pytest.fixture(scope='package')
def render_url():
    return f'http://{host}:{port}/render'


@pytest.fixture(scope='package')
def v0_refine_url():
    return f'http://{host}:{port}/v0/refine'


@pytest.fixture(scope='package')
def v0_render_url():
    return f'http://{host}:{port}/v0/render'


@pytest.fixture(scope='package')
def v1_render_url():
    return f'http://{host}:{port}/v1/render'
