import torch
import os.path as osp
from safetensors import safe_open
from safetensors.torch import save_file

tensors = {}
lora_path = osp.join(osp.join('models', 'loras', 'sd15-subj-keanur.safetensors'))
with safe_open(lora_path, framework='pt', device='cpu') as f:
   for key in f.keys():
       tensors[key] = f.get_tensor(key)
       print(key)
