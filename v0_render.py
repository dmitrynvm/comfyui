import os
import random
import sys
import time
from typing import Sequence, Mapping, Any, Union
import torch


def get_value_at_index(obj: Union[Sequence, Mapping], index: int) -> Any:
    try:
        return obj[index]
    except KeyError:
        return obj['result'][index]


def import_custom_nodes() -> None:
    import asyncio
    import execution
    from nodes import init_custom_nodes
    import server
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    server_instance = server.PromptServer(loop)
    execution.PromptQueue(server_instance)
    init_custom_nodes()


from nodes import (
    VAEDecode,
    EmptyLatentImage,
    LoraLoader,
    CLIPTextEncode,
    SaveImage,
    NODE_CLASS_MAPPINGS,
    CheckpointLoaderSimple,
    KSampler
)


def v0_render(
    session,
    uuid,
    size,
    steps,
    scale,
    seed,
    sampler,
    engine,
    adapter,
    positive,
    negative
):
    import_custom_nodes()
    with torch.inference_mode():
        checkpointloadersimple = CheckpointLoaderSimple()
        checkpointloadersimple_2 = checkpointloadersimple.load_checkpoint(
            ckpt_name=engine
        )

        emptylatentimage = EmptyLatentImage()
        emptylatentimage_3 = emptylatentimage.generate(
            width=size[0], height=size[1], batch_size=1
        )

        loraloader = LoraLoader()
        loraloader_12 = loraloader.load_lora(
            lora_name=adapter[0],
            strength_model=1,
            strength_clip=1,
            model=get_value_at_index(checkpointloadersimple_2, 0),
            clip=get_value_at_index(checkpointloadersimple_2, 1),
        )

        cliptextencode = CLIPTextEncode()
        cliptextencode_4 = cliptextencode.encode(
            text=positive,
            clip=get_value_at_index(loraloader_12, 1),
        )

        cliptextencode_5 = cliptextencode.encode(
            text=negative,
            clip=get_value_at_index(loraloader_12, 1),
        )

        ksampler = KSampler()
        vaedecode = VAEDecode()
        saveimage = SaveImage()


        ksampler_1 = ksampler.sample(
            seed=seed,
            steps=steps,
            cfg=scale,
            sampler_name=sampler,
            scheduler='normal',
            denoise=1,
            model=get_value_at_index(loraloader_12, 0),
            positive=get_value_at_index(cliptextencode_4, 0),
            negative=get_value_at_index(cliptextencode_5, 0),
            latent_image=get_value_at_index(emptylatentimage_3, 0),
        )

        vaedecode_6 = vaedecode.decode(
            samples=get_value_at_index(ksampler_1, 0),
            vae=get_value_at_index(checkpointloadersimple_2, 2),
        )

        saveimage_7 = saveimage.save_images(
            filename_prefix=f'{session}_render',
            images=get_value_at_index(vaedecode_6, 0),
        )
        del emptylatentimage
        del emptylatentimage_3
        del loraloader
        del loraloader_12
        del cliptextencode
        del cliptextencode_4
        del cliptextencode_5
        del ksampler
        del vaedecode
        del saveimage
        del vaedecode_6
        del ksampler_1
        del saveimage_7
        del checkpointloadersimple_2



if __name__ == '__main__':
    v0_render(
        session='pdagjn4',
        uuid=1294848,
        size=(512,512),
        steps=22,
        scale=7,
        seed=222222,
        sampler='dpmpp_2m',
        engine='sd15-jernau.safetensors',
        adapter=['sd15-lora-mrbean.safetensors'],
        positive='masterpiece, best quality), Arnold Schwarzenegger (as rastaman wearing rasta beanie), 1 man, epic (photo, studio lighting, hard light, sony a7, 50 mm, matte skin, pores, colors, hyperdetailed, hyperrealistic), ultrasharp, <lora:swarz:0.95>',
        negative='(nsfw:1.3), (nude:1.3), text, black and white, illustration, painting, cartoon, 3d, bad art, poorly drawn, close up, blurry, disfigured, deformed, extra limbs'
    )
